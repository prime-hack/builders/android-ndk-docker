# android-ndk-docker

Docker image with `android-ndk`. Its contain vanilla NDK (`android.toolchain.cmake`) and obfuscated (`android.obfuscated-toolchain.cmake`) toolchains.

Android-ndk directory is `/opt/android-ndk`

### Build

1. You must place needed files and directories to [install](install) directory.
2. You must export variable `DOCKER_BUILDKIT=1` to access to context on build stage
3. Execute `docker build /path/to/current/directory`
