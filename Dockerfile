# Require `export DOCKER_BUILDKIT=1` to build
FROM archlinux:latest

# Install packages
RUN --mount=target=/var/cache/pacman/pkg,type=cache --mount=target=/install,type=bind,source=install/pkgs \
	pacman -Syu --noconfirm && \
	pacman -S --noconfirm cmake make git python python2 && \
	pacman -U --noconfirm /install/*.pkg.tar.zst && \
	pacman -Scc --noconfirm

# Create obfuscated toolchain
RUN --mount=target=/install,type=bind,source=install/ollvm \
	sed 's/toolchains\/llvm\/prebuilt/toolchains\/ollvm\/prebuilt/g' /opt/android-ndk/build/cmake/android.toolchain.cmake > /opt/android-ndk/build/cmake/android.obfuscated-toolchain.cmake && \
	mkdir -p /opt/android-ndk/toolchains/ollvm/prebuilt/linux-x86_64/ && \
	ln -s /opt/android-ndk/toolchains/llvm/prebuilt/linux-x86_64/sysroot /opt/android-ndk/toolchains/ollvm/prebuilt/linux-x86_64/sysroot && \
	cp -r /install/* /opt/android-ndk/toolchains/ollvm/prebuilt/linux-x86_64/ && \
	rm -f /opt/android-ndk/toolchains/ollvm/prebuilt/linux-x86_64/README.md
